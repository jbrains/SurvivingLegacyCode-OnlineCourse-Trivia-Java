package ca.jbrains.trivia.test;

import com.adaptionsoft.games.uglytrivia.Game;
import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.core.Is.is;

public class IsGamePlayableTest {
    @Test
    public void noPlayers() throws Exception {
        Assert.assertThat(
                Game.wouldGameBePlayableWithThisManyPlayers(0),
                is(false));
    }

    @Test
    public void notEnoughPlayers() throws Exception {
        Assert.assertThat(
                Game.wouldGameBePlayableWithThisManyPlayers(1),
                is(false));
    }

    @Test
    public void enoughPlayers() throws Exception {
        Assert.assertThat(
                Game.wouldGameBePlayableWithThisManyPlayers(2),
                is(true));
    }

    @Test
    public void anUncomfortablyLargeNumberOfPlayers() throws Exception {
        Assert.assertThat(
                Game.wouldGameBePlayableWithThisManyPlayers(100),
                is(true));
    }
}
