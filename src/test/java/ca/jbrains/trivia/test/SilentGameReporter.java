package ca.jbrains.trivia.test;

import com.adaptionsoft.games.uglytrivia.GameReporter;

public class SilentGameReporter implements GameReporter {
    @Override
    public void reportMessage(Object message) {
        // Intentionally shut up!
    }
}
