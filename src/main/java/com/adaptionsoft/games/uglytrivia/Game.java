package com.adaptionsoft.games.uglytrivia;

import java.util.ArrayList;
import java.util.LinkedList;

public class Game {
    private final GameReporter gameReporter;
    public int[] places = new int[6];
    public int currentPlayer = 0;
    ArrayList players = new ArrayList();
    int[] purses = new int[6];
    boolean[] inPenaltyBox = new boolean[6];
    LinkedList popQuestions = new LinkedList();
    LinkedList scienceQuestions = new LinkedList();
    LinkedList sportsQuestions = new LinkedList();
    LinkedList rockQuestions = new LinkedList();
    boolean isGettingOutOfPenaltyBox;

    /**
     * @deprecated [Scheduled for removal 2017-03-01] Assumes that you want to report game progress to the console.
     */
    public Game() {
        this(new ConsoleGameReporter());
    }

    public Game(GameReporter gameReporter) {
        this.gameReporter = gameReporter;

        for (int i = 0; i < 50; i++) {
            popQuestions.addLast("Pop Question " + i);
            scienceQuestions.addLast(("Science Question " + i));
            sportsQuestions.addLast(("Sports Question " + i));
            rockQuestions.addLast(createRockQuestion(i));
        }
    }

    public String createRockQuestion(int index) {
        return "Rock Question " + index;
    }

    public boolean isPlayable() {
        // When you're ready, use wouldGameBePlayableWithThisManyPlayers(this.players.size())
        return (howManyPlayers() >= 2);
    }

    public static boolean wouldGameBePlayableWithThisManyPlayers(int howMany) {
        return (howMany >= 2);
    }

    public boolean add(String playerName) {


        players.add(playerName);
        places[howManyPlayers()] = 0;
        purses[howManyPlayers()] = 0;
        inPenaltyBox[howManyPlayers()] = false;

        reportMessage(playerName + " was added");
        reportMessage("They are player number " + players.size());
        return true;
    }

    public int howManyPlayers() {
        return players.size();
    }

    public void roll(int roll) {
        reportMessage(players.get(currentPlayer) + " is the current player");
        reportMessage("They have rolled a " + roll);

        if (inPenaltyBox[currentPlayer]) {
            if (roll % 2 != 0) {
                isGettingOutOfPenaltyBox = true;

                System.out.println(players.get(currentPlayer) + " is getting out of the penalty box");
                places[currentPlayer] = places[currentPlayer] + roll;
                if (places[currentPlayer] > 11) places[currentPlayer] = places[currentPlayer] - 12;

                System.out.println(players.get(currentPlayer)
                        + "'s new location is "
                        + places[currentPlayer]);
                System.out.println("The category is " + currentCategory());
                askQuestion();
            } else {
                System.out.println(players.get(currentPlayer) + " is not getting out of the penalty box");
                isGettingOutOfPenaltyBox = false;
            }

        } else {

            places[currentPlayer] = places[currentPlayer] + roll;
            if (places[currentPlayer] > 11) places[currentPlayer] = places[currentPlayer] - 12;

            reportMessage(players.get(currentPlayer)
                    + "'s new location is "
                    + places[currentPlayer]);
            reportMessage("The category is " + currentCategory());
            askQuestion();
        }

    }

    // SMELL askQuestion() uses removeFirst() and passes an Object
    // (not a String) to this method, and that is the ONLY REASON
    // that this method accepts an Object instead of a String.
    // One option: change the type of the questions lists so that
    // they explicitly mention that they store Strings.
    protected void reportMessage(Object message) {
        gameReporter.reportMessage(message);
    }

    private void askQuestion() {
        // REFACTOR Replace this with:
        // askQuestionForCategory(currentCategory(), popQuestions, scienceQuestions, sportsQuestions, rockQuestions, gameReporter);

        if (currentCategory() == "Pop")
            reportMessage(popQuestions.removeFirst());
        if (currentCategory() == "Science")
            reportMessage(scienceQuestions.removeFirst());
        if (currentCategory() == "Sports")
            reportMessage(sportsQuestions.removeFirst());
        if (currentCategory() == "Rock")
            reportMessage(rockQuestions.removeFirst());
    }

    // SMELL Using LinkedList for removeFirst() might be more clever than it's worth
    // REFACTOR Wrap question lists into a Parameter Object called QuestionDeck
    // REFACTOR Extract chooseQuestionForCategory(category, questionDeck) -> next question
    // REFACTOR asking the question would consist of simply reporting the text to the user,
    // then moving to the next question in that category for next time
    public static void askQuestionForCategory(String category, LinkedList popQuestions, LinkedList scienceQuestions, LinkedList sportsQuestions, LinkedList rockQuestions, GameReporter gameReporter) {
        if (category == "Pop")
            gameReporter.reportMessage(popQuestions.removeFirst());
        if (category == "Science")
            gameReporter.reportMessage(scienceQuestions.removeFirst());
        if (category == "Sports")
            gameReporter.reportMessage(sportsQuestions.removeFirst());
        if (category == "Rock")
            gameReporter.reportMessage(rockQuestions.removeFirst());
        // SMELL Silent default behavior might be confusing
    }

    private String currentCategory() {
        if (places[currentPlayer] == 0) return "Pop";
        if (places[currentPlayer] == 4) return "Pop";
        if (places[currentPlayer] == 8) return "Pop";
        if (places[currentPlayer] == 1) return "Science";
        if (places[currentPlayer] == 5) return "Science";
        if (places[currentPlayer] == 9) return "Science";
        if (places[currentPlayer] == 2) return "Sports";
        if (places[currentPlayer] == 6) return "Sports";
        if (places[currentPlayer] == 10) return "Sports";
        return "Rock";
    }

    public boolean wasCorrectlyAnswered() {
        if (inPenaltyBox[currentPlayer]) {
            // What does it mean to be "getting out of" the penalty box?!
            if (isGettingOutOfPenaltyBox) {
                // handle player answered question correctly
                System.out.println("Answer was correct!!!!");
                purses[currentPlayer]++;
                System.out.println(players.get(currentPlayer)
                        + " now has "
                        + purses[currentPlayer]
                        + " Gold Coins.");

                boolean winner = didPlayerWin();
                currentPlayer++;
                if (currentPlayer == players.size()) currentPlayer = 0;

                return winner;
            } else {
                // no need to handle player answered question correctly,
                // because the player is not "getting out of" the penalty box,
                // whatever that means

                currentPlayer++;
                if (currentPlayer == players.size()) currentPlayer = 0;
                return true;
            }


        } else {

            System.out.println("Answer was corrent!!!!");
            purses[currentPlayer]++;
            System.out.println(players.get(currentPlayer)
                    + " now has "
                    + purses[currentPlayer]
                    + " Gold Coins.");

            boolean winner = didPlayerWin();
            currentPlayer++;
            if (currentPlayer == players.size()) currentPlayer = 0;

            return winner;
        }
    }

    public boolean wrongAnswer() {
        System.out.println("Question was incorrectly answered");
        System.out.println(players.get(currentPlayer) + " was sent to the penalty box");
        inPenaltyBox[currentPlayer] = true;

        currentPlayer++;
        if (currentPlayer == players.size()) currentPlayer = 0;
        return true;
    }

    private boolean didPlayerWin() {
        return !(purses[currentPlayer] == 6);
    }

    public static class ConsoleGameReporter implements GameReporter {
        public void reportMessage(Object message) {
            System.out.println(message);
        }
    }
}
