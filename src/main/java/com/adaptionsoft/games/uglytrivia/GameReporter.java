package com.adaptionsoft.games.uglytrivia;

public interface GameReporter {
    void reportMessage(Object message);
}
