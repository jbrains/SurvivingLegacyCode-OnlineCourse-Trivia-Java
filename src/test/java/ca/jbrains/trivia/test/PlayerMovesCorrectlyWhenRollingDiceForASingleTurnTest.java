package ca.jbrains.trivia.test;

import com.adaptionsoft.games.uglytrivia.Game;
import com.adaptionsoft.games.uglytrivia.GameReporter;
import org.junit.Assert;
import org.junit.Test;

public class PlayerMovesCorrectlyWhenRollingDiceForASingleTurnTest {

    @Test
    public void fromTheStartingPlace() throws Exception {
        final int indexOfStartingPlace = 0;
        final int indexOfPlayerTakingATurn = 0;

        class GameReadyToTakeASingleTurn extends Game {
            public GameReadyToTakeASingleTurn(GameReporter gameReporter) {
                super(gameReporter);
                add("irrelevant player name");
                // SUPPOSE the first player is on place 0.
                super.places[indexOfPlayerTakingATurn] = indexOfStartingPlace;
                // SUPPOSE the next player to move is the first player
                super.currentPlayer = indexOfPlayerTakingATurn;
            }
        }
        final Game game = new GameReadyToTakeASingleTurn(new SilentGameReporter());

        // ASSUME it's the first player's turn, because we tried to make
        // it the first player's turn
        game.roll(1);

        Assert.assertEquals(1, game.places[indexOfPlayerTakingATurn]);
    }

    @Test
    public void fromTheStartingToPlaceToSomewhereElse() throws Exception {
        final int indexOfStartingPlace = 0;
        final int indexOfPlayerTakingATurn = 0;

        class GameReadyToTakeASingleTurn extends Game {
            public GameReadyToTakeASingleTurn(GameReporter gameReporter) {
                super(gameReporter);
                add("irrelevant player name");
                // SUPPOSE the first player is on place 0.
                super.places[indexOfPlayerTakingATurn] = indexOfStartingPlace;
                // SUPPOSE the next player to move is the first player
                super.currentPlayer = indexOfPlayerTakingATurn;
            }
        }
        final Game game = new GameReadyToTakeASingleTurn(new SilentGameReporter());

        // ASSUME it's the first player's turn, because we tried to make
        // it the first player's turn
        game.roll(8);

        Assert.assertEquals(8, game.places[indexOfPlayerTakingATurn]);
    }

    @Test
    public void fromNotTheStartingPlace() throws Exception {
        final int indexOfWhereThePlayerStarts = 4;
        final int indexOfPlayerTakingATurn = 0;

        class GameReadyToTakeASingleTurn extends Game {
            public GameReadyToTakeASingleTurn(GameReporter gameReporter) {
                super(gameReporter);
                add("irrelevant player name");
                // SUPPOSE the first player is on place 0.
                super.places[indexOfPlayerTakingATurn] = indexOfWhereThePlayerStarts;
                // SUPPOSE the next player to move is the first player
                super.currentPlayer = indexOfPlayerTakingATurn;
            }
        }
        final Game game = new GameReadyToTakeASingleTurn(new SilentGameReporter());

        // ASSUME it's the first player's turn, because we tried to make
        // it the first player's turn
        game.roll(3);

        Assert.assertEquals(7, game.places[indexOfPlayerTakingATurn]);
    }

    @Test
    public void aroundTheEndOfTheBoard() throws Exception {
        final int indexOfWhereThePlayerStarts = 11;
        final int indexOfPlayerTakingATurn = 0;

        class GameReadyToTakeASingleTurn extends Game {
            public GameReadyToTakeASingleTurn(GameReporter gameReporter) {
                super(gameReporter);
                add("irrelevant player name");
                // SUPPOSE the first player is on place 0.
                super.places[indexOfPlayerTakingATurn] = indexOfWhereThePlayerStarts;
                // SUPPOSE the next player to move is the first player
                super.currentPlayer = indexOfPlayerTakingATurn;
            }
        }
        final Game game = new GameReadyToTakeASingleTurn(new SilentGameReporter());

        // ASSUME it's the first player's turn, because we tried to make
        // it the first player's turn
        game.roll(3);

        Assert.assertEquals(2, game.places[indexOfPlayerTakingATurn]);
    }
}
