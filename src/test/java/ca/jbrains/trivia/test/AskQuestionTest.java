package ca.jbrains.trivia.test;

import com.adaptionsoft.games.uglytrivia.Game;
import com.adaptionsoft.games.uglytrivia.GameReporter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.NoSuchElementException;

import static org.hamcrest.core.Is.is;

public class AskQuestionTest {
    private String lastQuestionAsked = null;
    private GameReporter spyGameReporter;

    @Before
    public void before() {
        spyGameReporter = new GameReporter() {
            @Override
            public void reportMessage(Object message) {
                AskQuestionTest.this.lastQuestionAsked = (String) message;
            }
        };
    }

    @Test
    public void popQuestion() throws Exception {
        final LinkedList<String> popQuestions = new LinkedList<String>(Arrays.asList(
                "::the next question in the Pop category::"));

        Game.askQuestionForCategory("Pop", popQuestions, null, null, null, spyGameReporter);

        Assert.assertThat(
                lastQuestionAsked,
                is("::the next question in the Pop category::"));
    }

    @Test
    public void scienceQuestion() throws Exception {
        final LinkedList<String> scienceQuestions = new LinkedList<String>(Arrays.asList(
                "::the next question in the Science category::"));

        Game.askQuestionForCategory("Science", null, scienceQuestions, null, null, spyGameReporter);

        Assert.assertThat(
                lastQuestionAsked,
                is("::the next question in the Science category::"));
    }

    @Test
    public void sportsQuestion() throws Exception {
        final LinkedList<String> sportsQuestions = new LinkedList<String>(Arrays.asList(
                "::the next question in the Sports category::"));

        Game.askQuestionForCategory("Sports", null, null, sportsQuestions, null, spyGameReporter);

        Assert.assertThat(
                lastQuestionAsked,
                is("::the next question in the Sports category::"));
    }


    @Test
    public void rockQuestion() throws Exception {
        final LinkedList<String> rockQuestions = new LinkedList<String>(Arrays.asList(
                "::the next question in the Rock category::"));

        Game.askQuestionForCategory("Rock", null, null, null, rockQuestions, spyGameReporter);

        Assert.assertThat(
                lastQuestionAsked,
                is("::the next question in the Rock category::"));
    }

    @Test
    public void noQuestionsLeftInTheCategory() throws Exception {
        // SMELL "Rock" is an irrelevant detail in this test
        // REFACTOR This sounds like a test for a single list of questions, and not a QuestionDeck

        final LinkedList<String> noMoreRockQuestions = new LinkedList<String>(Collections.<String>emptyList());

        try {
            Game.askQuestionForCategory("Rock", null, null, null, noMoreRockQuestions, null);
            Assert.fail("How did you ask a question in the Rock category when there aren't any left?!");
        } catch (NoSuchElementException expected) {
        }
    }

    @Test
    public void twoConsecutiveQuestionsInTheSameCategory() throws Exception {
        // REFACTOR This looks like a test for a single category of questions, and
        // not a QuestionDeck or something larger.

        final LinkedList<String> rockQuestions = new LinkedList<String>(Arrays.asList(
                "::the question we expect to ask the first time::",
                "::the question we expect to ask the second time::"));

        Game.askQuestionForCategory("Rock", null, null, null, rockQuestions, spyGameReporter);

        Assert.assertThat(
                lastQuestionAsked,
                is("::the question we expect to ask the first time::"));

        Game.askQuestionForCategory("Rock", null, null, null, rockQuestions, spyGameReporter);

        Assert.assertThat(
                lastQuestionAsked,
                is("::the question we expect to ask the second time::"));
    }
}
