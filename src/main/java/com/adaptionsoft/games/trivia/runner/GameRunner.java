package com.adaptionsoft.games.trivia.runner;

import com.adaptionsoft.games.uglytrivia.Game;

import java.util.Random;

// SMELL Most of this code looks like it belongs in Game,
// so why is it in GameRunner?!
public class GameRunner {
    // SMELL Storing a boolean value backwards from what I would expect.
    // Why is this now simply "playerHasWon"?
    private static boolean notAWinner;

    public static void main(String[] args) {
        // SMELL Game uses the plugged-in GameReporter some of the time and
        // the default (ConsoleGameReporter) the rest of the time,
        // so we are unable to use the new constructor here.
        Game aGame = new Game();

        aGame.add("Chet");
        aGame.add("Pat");
        aGame.add("Sue");

        Random rand = new Random();

        do {
            aGame.roll(rand.nextInt(5) + 1);

            if (rand.nextInt(9) == 7) {
                notAWinner = aGame.wrongAnswer();
            } else {
                notAWinner = aGame.wasCorrectlyAnswered();
            }
        } while (notAWinner);
    }
}
