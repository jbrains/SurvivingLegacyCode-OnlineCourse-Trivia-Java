# Surviving Legacy Code Online Course

This code base accompanies the online training course [Surviving Legacy Code](http://surviving-legacy-code.jbrains.ca). You can probably learn a lot from reading the commits in this code base, but if you don't sign up for the course, then I can't make any guarantee about what you will learn from it. Good luck.

## Pull Requests

This is not an active development project, so I'm not looking for contributions. I can't think of a situation in which I would accept a pull request, but you creative people might surprise me. If you are enrolled in the course and would like me to review some of your work, then point me to a commit and some explanation of what you've tried to do, and I'll share my thoughts. If you are not enrolled in the course, then I might share my thoughts, if I have the time and energy. (I never know when that might be.)

## Tags

I have tried to add tags that will help you find the code that corresponds to each lecture. This will not necessarily be complete nor perfectly accurate, but it should help. It is more important for you to try these techniques on your own code (or your own version of my code) than it is to read my code and know precisely which commits match which videos. I tried.
