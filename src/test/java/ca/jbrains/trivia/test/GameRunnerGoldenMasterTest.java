package ca.jbrains.trivia.test;

import com.adaptionsoft.games.uglytrivia.Game;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Random;

// Intentionally duplicates production GameRunner first now.
public class GameRunnerGoldenMasterTest {
    private static boolean notAWinner;

    public static void main(String[] args) throws IOException {
        final PrintStream systemOut = System.out;

        final int howManyTestRuns = 1000;

        final int startingGameId = 7777;
        final int arbitraryGameIdOffset = 13;

        // REFACTOR Make the current working directory more explicit?
        final File testDataWorkspaceDirectory = new File("test-data");

        final File testRunDirectory = new File(testDataWorkspaceDirectory, "test-run");

        // REFACTOR Use Apache Commons IO when that becomes available to us:
        // FileUtils.cleanDirectory(directory);
        for (File each : testRunDirectory.listFiles())
            if (!each.isDirectory())
                each.delete();

        for (int i = 0; i < howManyTestRuns; i++) {
            final int gameId = startingGameId + i * arbitraryGameIdOffset;

            final File testRunOutputFile = new File(
                    testRunDirectory,
                    String.format("game-%d.txt", gameId));

            // REFACTOR Make the game output stream pluggable, since clearly, I want to plug
            // something else into it.
            System.setOut(new PrintStream(testRunOutputFile));

            // SMELL Game uses the plugged-in GameReporter some of the time and
            // the default (ConsoleGameReporter) the rest of the time,
            // so we are unable to use the new constructor here.
            Game aGame = new Game();

            aGame.add("Chet");
            aGame.add("Pat");
            aGame.add("Sue");

            // We can conveniently use the game ID directly as a random number generator seed.
            Random rand = new Random(gameId);

            do {
                aGame.roll(rand.nextInt(5) + 1);

                if (rand.nextInt(9) == 7) {
                    notAWinner = aGame.wrongAnswer();
                } else {
                    notAWinner = aGame.wasCorrectlyAnswered();
                }
            } while (notAWinner);
        }

        System.setOut(systemOut);
    }
}
